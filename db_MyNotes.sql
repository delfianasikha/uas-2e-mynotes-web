/*
SQLyog Professional
MySQL - 10.1.37-MariaDB : Database - uas_cb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`uas_cb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `uas_cb`;

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`id_kategori`,`nama_kategori`) values 
(2,'diary'),
(4,'hangout'),
(7,'tugas');

/*Table structure for table `notes` */

DROP TABLE IF EXISTS `notes`;

CREATE TABLE `notes` (
  `id_notes` int(11) NOT NULL AUTO_INCREMENT,
  `nama_notes` varchar(150) NOT NULL,
  `photos` varchar(50) NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_notes`),
  KEY `id_kategori` (`id_kategori`),
  CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `notes` */

insert  into `notes`(`id_notes`,`nama_notes`,`photos`,`id_kategori`) values 
(1,'saya','ayu.jpg',2),
(5,'uas mobile','DC20200517125853.jpg',7),
(7,'contoh','khoirul.jpg',4);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` varchar(11) NOT NULL,
  `pass` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id_user`,`pass`) values 
('puja','puja'),
('saya','saya');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
